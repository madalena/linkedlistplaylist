package com.mk;

import java.util.ArrayList;

public class Album {
    private String albumName;
    private String bandName;
    private SongList songList;

    public Album(String albumName, String bandName) {
        this.albumName = albumName;
        this.bandName = bandName;
        this.songList = new SongList();
    }

    public void addSongToAlbum(String title, String duration) {
        this.songList.addSongToAlbum(title, duration);
    }

    public Song getSongByTitle(String title) {
        for(int i = 0; i< this.songList.arrayList.size(); i++) {
        if(this.songList.arrayList.get(i).getTitle().equals(title))
            return this.songList.arrayList.get(i);
        }
        return null;
    }

    public void printAlbum() {
        System.out.println("Band name: " + this.bandName + ", Album: " + this.albumName);
        for(int i = 0; i < songList.arrayList.size(); i++) {
            System.out.println(i+1 + ". " + songList.arrayList.get(i));
        }
    }

    //-----------------------------------------------------------------------------------------

    private class SongList {
        private ArrayList<Song> arrayList;

        public SongList() {
            this.arrayList = new ArrayList<>();
        }

        public void addSongToAlbum(String songTitle, String duration) {
            if(!findSong(songTitle)) {
                Song newSong = new Song(songTitle, duration);
                this.arrayList.add(newSong);
            }
            else {
                System.out.println("The song " + songTitle + " is already in this album.");
            }
        }

        public boolean findSong(String songTitle) {
            for(int i = 0; i< this.arrayList.size(); i++) {
                if(this.arrayList.get(i).equals(songTitle)) {
                    return true;
                }
            }
            return false;
        }
    }

}
