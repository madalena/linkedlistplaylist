package com.mk;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        LinkedList<Song> playlist = new LinkedList<>();

        Album pinkFloyd = addTheDarkSideOfTheMoon();
        Album radiohead = addOKComputer();

        //adding songs to playlist:
        playlist.add(pinkFloyd.getSongByTitle("Eclipse"));
        playlist.add(radiohead.getSongByTitle("No Surprises"));
        playlist.add(radiohead.getSongByTitle("Karma Police"));
        playlist.add(pinkFloyd.getSongByTitle("Time"));
        playlist.add(pinkFloyd.getSongByTitle("Us and them"));

        //test printing
        printList(playlist);

        listenPlaylist(playlist);
    }

    //--------------------------------------------------------------------------------

    private static void printMenu() {
        System.out.println("Choose:" +
                "0 - quit listening playlist\n" +
                "1 - skip forward to the next song\n" +
                "2 - skip backwards to a previous song\n" +
                "3 - replay the current song\n" +
                "4 - remove current song\n" +
                "5 - print playlist\n" +
                "6 - print menu\n");
    }

    private static void listenPlaylist(LinkedList<Song> playlist) {
        Scanner scanner = new Scanner(System.in);
        boolean quit = false;
        boolean goingForward = true;
        ListIterator<Song> listIterator = playlist.listIterator();

        if(playlist.getFirst() == null) {
            System.out.println("No songs in playlist.");
            return;
        }
        printMenu();
        Song next = listIterator.next();
        System.out.println("Now playing: " + next.getTitle() + "\t" + next.getDuration());

        while(!quit) {
            int action = scanner.nextInt();
            scanner.nextLine();

            switch(action) {
                case 0:
                    System.out.println("Closing playlist.");
                    quit = true;
                    break;
                case 1:
                    if(!goingForward) {
                        if(listIterator.hasNext()) {
                            listIterator.next();
                        }
                        goingForward = true;
                    }
                    if(listIterator.hasNext()) {
                        next = listIterator.next();
                        System.out.println("Now playing " + next.getTitle() + "\t" + next.getDuration());
                    }
                    else {
                        System.out.println("Reached the end of the playlist");
                    }
                    break;

                case 2:
                    if(goingForward) {
                        if (listIterator.hasPrevious()) {
                            listIterator.previous();
                        }
                        goingForward = false;
                    }
                    if(listIterator.hasPrevious()) {
                        next = listIterator.previous();
                        System.out.println("Now playing " + next.getTitle() + "\t" + next.getDuration());
                    } else {
                        System.out.println("Reached the start of the playlist");
                    }
                    break;

                case 3:
                    if(goingForward) {
                        if (listIterator.hasPrevious()) {
                            next = listIterator.previous();
                            System.out.println("Now replaying " + next.getTitle() + "\t" + next.getDuration());
                            goingForward = false;
                        } else {
                            System.out.println("Reached the start of the playlist");
                        }
                    }
                    else {
                        if(listIterator.hasNext()) {
                            next = listIterator.next();
                            System.out.println("Now replaying " + next.getTitle() + "\t" + next.getDuration());
                            goingForward = true;
                        }
                        else {
                            System.out.println("Reached the end of the playlist.");
                        }
                    }
                    break;

                case 4:
                    if(playlist.size() > 0) {
                        listIterator.remove();
                        if(listIterator.hasNext()) {
                            next = listIterator.next();
                            System.out.println("Now playing: " + next.getTitle() + "\t" + next.getDuration());
                        }
                        else if(listIterator.hasPrevious()) {
                            next = listIterator.previous();
                            System.out.println("Now playing: " + next.getTitle() + "\t" + next.getDuration());
                        }
                    }
                    else {
                        System.out.println("No more songs to delete.");
                    }
                    break;

                case 5:
                    printList(playlist);
                    break;

                case 6:
                    printMenu();
                    break;
            }
        }
    }

    private static void printList(LinkedList<Song> playlist) {
        Iterator<Song> i = playlist.iterator();
        int j = 1;

        while(i.hasNext()) {
            Song next = i.next(); //temp
            System.out.println(j++ + ": " + next.getTitle() + "\t\t" + next.getDuration());
        }
        System.out.println("-----------------------------");
    }

    //--------------------------------------------------------------------------------
    //Add albums base

    private static Album addTheDarkSideOfTheMoon() {
        Album pinkFloyd = new Album("Dark Side of the Moon", "Pink Floyd");

        pinkFloyd.addSongToAlbum("Speak to Me/Breathe", "3:57");
        pinkFloyd.addSongToAlbum("On the Run", "3:50");
        pinkFloyd.addSongToAlbum("Time", "6:49");
        pinkFloyd.addSongToAlbum("The Great Gig in the Sky", "4:44");
        pinkFloyd.addSongToAlbum("Money", "6:22");
        pinkFloyd.addSongToAlbum("Us and them", "7:49");
        pinkFloyd.addSongToAlbum("Any colour you like", "3:26");
        pinkFloyd.addSongToAlbum("Brain Damage", "3:46");
        pinkFloyd.addSongToAlbum("Eclipse", "2:11");

        return pinkFloyd;
    }

    private static Album addOKComputer() {
        Album radiohead = new Album("OK Computer", "Radiohead");

        radiohead.addSongToAlbum("Airbag", "4:44");
        radiohead.addSongToAlbum("Paranoid Android", "6:23");
        radiohead.addSongToAlbum("Subterranean Homesick Alien", "4:28");
        radiohead.addSongToAlbum("Exit Music", "4:24");
        radiohead.addSongToAlbum("Let Down", "4:59");
        radiohead.addSongToAlbum("Karma Police", "4:21");
        radiohead.addSongToAlbum("Fitter Happier", "1:58");
        radiohead.addSongToAlbum("No Surprises", "3:48");
        //...

        return radiohead;
    }
}
